package com.ghy.centerinfo.task.contract.iface;

import com.ghy.centerinfo.task.contract.model.ScheduleJobLogEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 定时任务日志
 *
 */
@RequestMapping(value = "/ScheduleJobLogService")
public interface ScheduleJobLogService {

    /**
     * 根据ID，查询定时任务日志
     */
    @GetMapping(value = "/queryObject")
    ScheduleJobLogEntity queryObject(@RequestParam("jobId") Long jobId);

    /**
     * 查询定时任务日志列表
     */
    @GetMapping(value = "/queryList")
    List<ScheduleJobLogEntity> queryList(@RequestBody Map<String, Object> map);

    /**
     * 查询总数
     */
    @GetMapping(value = "/queryTotal")
    int queryTotal(@RequestBody Map<String, Object> map);

    /**
     * 保存定时任务日志
     */
    @PostMapping(value = "/save")
    int save(@RequestBody ScheduleJobLogEntity log);

}
