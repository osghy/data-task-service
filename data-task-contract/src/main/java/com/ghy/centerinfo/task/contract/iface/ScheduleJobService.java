package com.ghy.centerinfo.task.contract.iface;

import com.ghy.centerinfo.task.contract.model.ScheduleJobEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 定时任务
 *
 */
@RequestMapping("/ScheduleJobService")
public interface ScheduleJobService {

    /**
     * 根据ID，查询定时任务
     */
    @GetMapping("/queryObject")
    ScheduleJobEntity queryObject(@RequestParam("jobId") Long jobId);

    /**
     * 查询定时任务列表
     */
    @RequestMapping(value = "/queryList", method = RequestMethod.POST)
    List<ScheduleJobEntity> queryList(@RequestBody Map<String, Object> map);

    /**
     * 查询总数
     */
    @GetMapping("/queryTotal")
    int queryTotal(@RequestBody Map<String, Object> map);

    /**
     * 保存定时任务
     */
    @PostMapping("/save")
    int save(@RequestBody ScheduleJobEntity scheduleJob);

    /**
     * 更新定时任务
     */
    @PostMapping("/update")
    int update(@RequestBody ScheduleJobEntity scheduleJob);

    /**
     * 批量删除定时任务
     */
    @PostMapping("/deleteBatch")
    boolean deleteBatch(@RequestParam("jobIds") Long[] jobIds);

    /**
     * 批量更新定时任务状态
     */
    @PostMapping("updateBatch")
    int updateBatch(@RequestParam("jobIds") Long[] jobIds, @RequestParam("status") int status);

    /**
     * 立即执行
     */
    @PostMapping("run")
    boolean run(@RequestParam("jobIds") Long[] jobIds);

    /**
     * 暂停运行
     */
    @PostMapping("pause")
    boolean pause(@RequestParam("jobIds") Long[] jobIds);

    /**
     * 恢复运行
     */
    @PostMapping("resume")
    boolean resume(@RequestParam("jobIds") Long[] jobIds);
}
