package task;

import base.BaseTests;
import com.ghy.centerinfo.task.contract.iface.ScheduleJobService;
import com.ghy.centerinfo.task.contract.model.ScheduleJobEntity;
import com.ghy.centerinfo.task.service.ScheduleJobServiceImpl;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * create by huweiqiang on 2019/5/5
 */
@SpringBootTest(classes = ScheduleJobServiceImpl.class)
@ContextConfiguration(locations = {"classpath:spring.xml"})
public class TestTask extends BaseTests {

    @Autowired
    private ScheduleJobService scheduleJobService;

    @Test
    public void testSave(){
        ScheduleJobEntity entity = new ScheduleJobEntity();
        entity.setBeanName("testScheduler");
        entity.setCronExpression("0 0/1 * * * ? ");
        entity.setMethodName("testMethod");
        entity.setRemark("测试任务");
        scheduleJobService.save(entity);
    }

    @Test
    public void  testQueryList(){

        Map<String,Object> map = new HashMap<>();
        List<ScheduleJobEntity> list = scheduleJobService.queryList(map);
        System.out.println(list.get(0).getBeanName());
    }

}
