package com.ghy.centerinfo.task.service;

import com.ghy.centerinfo.task.contract.iface.ScheduleJobService;
import com.ghy.centerinfo.task.contract.model.ScheduleJobEntity;
import com.ghy.centerinfo.task.dao.ScheduleJobDao;
import com.ghy.centerinfo.task.domain.manager.QuartzJobManager;
import com.ghy.centerinfo.task.domain.mapper.ScheduleJobMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * create by huweiqiang on 2019/4/30
 */
@RestController
public class ScheduleJobServiceImpl implements ScheduleJobService {

    private static final Logger logger = LoggerFactory.getLogger(ScheduleJobServiceImpl.class);
    @Autowired
    private QuartzJobManager quartzJobManager;
    @Autowired
    private ScheduleJobMapper scheduleJobMapper;


    @Override
    public ScheduleJobEntity queryObject(@RequestParam("jobId") Long jobId) {
        return null;
    }

    @Override
    public List<ScheduleJobEntity> queryList(@RequestBody Map<String, Object> map) {

        return scheduleJobMapper.queryList(map);
    }


    @Override
    public int queryTotal(@RequestBody Map<String, Object> map) {
        return 0;
    }

    @Override
    public int save(@RequestBody ScheduleJobEntity scheduleJob) {
        return quartzJobManager.save(scheduleJob);
    }

    @Override
    public int update(@RequestBody ScheduleJobEntity scheduleJob) {

        return 0;
    }

    @Override
    public boolean deleteBatch(@RequestParam("jobIds") Long[] jobIds) {
        return false;
    }

    @Override
    public int updateBatch(@RequestParam("jobIds") Long[] jobIds,@RequestParam("status")  int status) {
        return 0;
    }

    @Override
    public boolean run(@RequestParam("jobIds")  Long[] jobIds) {
        return false;
    }

    @Override
    public boolean pause(@RequestParam("jobIds") Long[] jobIds) {
        return false;
    }

    @Override
    public boolean resume(@RequestParam("jobIds") Long[] jobIds) {
        return false;
    }

}
