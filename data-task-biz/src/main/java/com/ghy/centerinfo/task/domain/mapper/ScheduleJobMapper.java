package com.ghy.centerinfo.task.domain.mapper;


import com.ghy.centerinfo.task.contract.model.ScheduleJobEntity;
import com.ghy.centerinfo.task.dao.BaseDao;
import org.springframework.stereotype.Repository;

/**
 * create by huweiqiang on 2019/4/29
 */
@Repository
public interface ScheduleJobMapper extends BaseDao<ScheduleJobEntity> {


}
