package com.ghy.centerinfo.task.dao;

import com.ghy.centerinfo.task.contract.model.ScheduleJobEntity;
import com.ghy.centerinfo.task.domain.mapper.ScheduleJobMapper;
import com.ghy.ic.commons.persistence.DBSwitch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Map;

/**
 * create by huweiqiang on 2019/4/29
 */
@Repository
public class ScheduleJobDao {

    @Autowired
    private ScheduleJobMapper scheduleJobMapper;

    public ScheduleJobEntity queryScheJob(int jobId){
        Assert.isTrue(jobId > 0,"========>jobId不能为空！");
        return  scheduleJobMapper.queryObject(jobId);
    }

    @DBSwitch("scheduler")
    public int save(ScheduleJobEntity entity){
        return scheduleJobMapper.save(entity);
    }

}
