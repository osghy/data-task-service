package com.ghy.centerinfo.task.domain.manager;

import com.ghy.centerinfo.task.contract.enums.Constant;
import com.ghy.centerinfo.task.contract.model.ScheduleJobEntity;
import com.ghy.centerinfo.task.dao.ScheduleJobDao;
import com.ghy.centerinfo.task.util.ScheduleUtils;
import com.ghy.ic.commons.persistence.DBSwitch;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * create by huweiqiang on 2019/4/28
 */
@Service
public class QuartzJobManager {
    private static final Logger logger = LoggerFactory.getLogger(QuartzJobManager.class);
    @Autowired
    private ScheduleJobDao scheduleJobDao;
    @Autowired
    private Scheduler scheduler;

    @DBSwitch("scheduler")
    @Transactional
    public int save(ScheduleJobEntity scheduleJob) {
        scheduleJob.setCreateTime(new Date());
        scheduleJob.setStatus(Constant.ScheduleStatus.NORMAL.getValue());
        int insertCount = scheduleJobDao.save(scheduleJob);
        if(insertCount > 0 ){
            ScheduleUtils.createScheduleJob(scheduler, scheduleJob);
        }
        return insertCount;
    }
}

