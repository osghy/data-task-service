package com.ghy.centerinfo.task.job;

import org.springframework.stereotype.Component;

/**
 * create by huweiqiang on 2019/5/5
 */
@Component("testScheduler")
public class TestScheduler {

    //利用Java反射执行方法
    public void testMethod(){

        System.out.println("=====>开始执行新任务，五一假期已结束，加油.............");
    }

    //利用Java反射执行方法
    public void testMethod2() {

        System.out.println("=====>十分钟执行一次.............");
    }

    //利用Java反射执行方法
    public void testMethod3() {

        System.out.println("=====>每天0点执行.............");
    }
}
