package com.ghy.centerinfo.task.service;

import com.ghy.centerinfo.task.contract.iface.ScheduleJobLogService;
import com.ghy.centerinfo.task.contract.model.ScheduleJobLogEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * create by huweiqiang on 2019/4/30
 */
@Component("scheduleJobLogService")
public class ScheduleJobLogServiceImpl implements ScheduleJobLogService {


    @Override
    public ScheduleJobLogEntity queryObject(@RequestParam("jobId") Long jobId) {
        return null;
    }

    @Override
    public List<ScheduleJobLogEntity> queryList(@RequestBody Map<String, Object> map) {
        return null;
    }

    @Override
    public int queryTotal(@RequestBody Map<String, Object> map) {
        return 0;
    }

    @Override
    public int save(@RequestBody ScheduleJobLogEntity log) {
        return 0;
    }
}
