package com.ghy.centerinfo.task.dao;

import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * create by huweiqiang on 2019/4/29
 */
@Repository
public interface BaseDao<T> {

    int save(T t);

    void save(Map<String, Object> map);

    void saveBatch(List<T> list);

    int update(T t);

    int update(Map<String, Object> map);

    int delete(Object id);

    int delete(Map<String, Object> map);

    int deleteBatch(Object[] id);

    T queryObject(Object id);

    List<T> queryList(Map<String, Object> map);

    List<T> queryList(Object id);

    int queryTotal(Map<String, Object> map);

    int queryTotal();

}
